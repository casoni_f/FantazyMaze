#!/usr/bin/python
# -*- coding: utf8 -*-

import pygame


class SpritesImporter:
    def __init__(self, filename):
        self.sheet = pygame.image.load(filename).convert()

    def get_image_at(self, rectangle, color_key=None):
        rect = pygame.Rect(rectangle)
        image = pygame.Surface(rect.size).convert()
        image.blit(self.sheet, (0, 0), rect)
        if color_key is not None:
            if color_key is -1:
                color_key = image.get_at((0, 0))
            image.set_colorkey(color_key, pygame.RLEACCEL)
        return image

    def get_images_at(self, rects, color_key=None):
        return [self.get_image_at(rect, color_key) for rect in rects]

    def load_strip(self, rect, image_count, color_key=None):
        tups = [(rect[0] + rect[2] * x, rect[1], rect[2], rect[3])
                for x in range(image_count)]
        return self.get_images_at(tups, color_key)
