#!/usr/bin/python
# -*- coding: utf8 -*-

from config import *
import random
import pygame


class Trap:
    def __init__(self, x, y, activated=True):
        self.activated = activated
        self.x = x
        self.y = y
        self.sprite = None

    def on_trigger(self, player, level):
        pass


class ExplosiveTrap(Trap):
    def on_trigger(self, player, level):
        player.case_x = 1
        player.case_y = 1
        player.x = player.case_x * SPRITE_SIZE
        player.y = player.case_y * SPRITE_SIZE
        self.activated = False


class RandomizeTrap(Trap):
    def on_trigger(self, player, level):
        x = random.randrange(1, 30)
        y = random.randrange(1, 30)
        while level.data[y][x] == 'w':
            x = random.randrange(1, 30)
            y = random.randrange(1, 30)
        player.case_x = x
        player.case_y = y
        player.x = player.case_x * SPRITE_SIZE
        player.y = player.case_y * SPRITE_SIZE
        self.activated = False


class ConfusingTrap(Trap):
    def on_trigger(self, player, level):
        player.confused += 10
        self.activated = False


class SnakeTrap(Trap):
    def on_trigger(self, player, level):
        player.poison = 20
        antidote_coord = random.choice(level.free_cases)
        level.free_cases.remove(antidote_coord)
        level.data[antidote_coord[1]][antidote_coord[0]] = 'a'
        self.activated = False
        self.sprite = SNAKE_TRAP_ACTIVATED


class SpikeTrap(Trap):
    def on_trigger(self, player, level):
        player.actual_hp -= 1
        self.activated = False
        self.sprite = SPIKE_TRAP_ACTIVATED
