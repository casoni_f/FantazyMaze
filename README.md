# Fantazy Maze

## Requirements
* Python 3
* Install requirements.txt (pip install -r requirements.txt)

## How To
* Execute maze.py
* Select a hero with numpad 0-9 (for the moment 1 = DK, 2 = Link and 3 = Happy)
* Select a level with F1-F12 (for the moment 6 levels are available)
* Or press E for the Endless Mode
* Use Arrows or ZQSD to move
* Enjoy !

## Keybinds
* Z/W or UpArrow to move up
* S or DownArrow to move down
* Q or LeftArrow to move left
* D or RightArrow to move right
* M to go back to Start Menu
* E when on Start Menu to start the Endless Mode
* Escape to exit the game
* P to mute sounds
* +/- to change the volume

## Endless Mode
In Endless Mode, you'll have to reach the end of the level while getting the maximum number of bonuses.

Each bonus can grant you 10, 25 or 50 points. Levels will go on endlessly until the timer reaches 0 or your hp total reaches 0.

But be careful, some traps are waiting for you !

## Traps
Here is a list of the different traps you will face:
* Explosive Trap (not in Endless Mode): It will send you back to the start of the level in a explosion.

* Snake Trap : Evil snakes will bite you and poison you. An antidote will appear in the maze, try to grab it before it's too late and you lose 1 hp.

* Confusing Trap (not in Endless Mode): The loneliness (and maybe the gaz of the trap) drives you crazy and you won't move as you want for a bit.

* Portal Trap (not in Endless Mode): The portal will teleport you somewhere else randomly ! (I hope you like dead ends !)

* Spike Trap : All adventurers know this trap. -1 Hp for you sir, now keep going !