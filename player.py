#!/usr/bin/python
# -*- coding: utf8 -*-

import os
import random
from spritesheet import *
from config import *


class Player:
    def __init__(self, level, hero, score=0):
        self.max_hp = 3
        self.actual_hp = 3
        self.poison = -1
        self.x = level.init_x * SPRITE_SIZE
        self.y = level.init_y * SPRITE_SIZE
        self.case_x = level.init_x
        self.case_y = level.init_y
        self.confused = 0
        self.score = score
        self.highscore = self.load_highscore()
        if hero[1]:
            self.set_hero_from_spritesheet(hero[0])
        else:
            self.set_hero_sprites(hero[0])
        self.side = self.right
        self.level = level.data

    def set_hero_from_spritesheet(self, hero):
        s = SpritesImporter(SHEETS_PATH + hero + '_sheet.png')
        rects = []
        rects.append((0, 0, 120, 140))
        rects.append((0, 130, 120, 140))
        rects.append((0, 260, 120, 140))
        rects.append((0, 360, 120, 140))
        sprites = s.get_images_at(rects, (0, 0, 0,))
        self.down = pygame.transform.smoothscale(sprites[0], (SPRITE_SIZE, SPRITE_SIZE))
        self.left = pygame.transform.smoothscale(sprites[1], (SPRITE_SIZE, SPRITE_SIZE))
        self.up = pygame.transform.smoothscale(sprites[2], (SPRITE_SIZE, SPRITE_SIZE))
        self.right = pygame.transform.smoothscale(sprites[3], (SPRITE_SIZE, SPRITE_SIZE))

    def set_hero_sprites(self, hero):
        hero_left = SPRITES_PATH + hero + '/left.png'
        hero_right = SPRITES_PATH + hero + '/right.png'
        hero_up = SPRITES_PATH + hero + '/up.png'
        hero_down = SPRITES_PATH + hero + '/down.png'
        self.left = pygame.image.load(hero_left).convert_alpha()
        self.right = pygame.image.load(hero_right).convert_alpha()
        self.up = pygame.image.load(hero_up).convert_alpha()
        self.down = pygame.image.load(hero_down).convert_alpha()

    def do(self, key):
        if key in MOVE_KEYS:
            if self.poison > 0:
                self.poison -= 1
            self.move(key)
        else:
            self.do_action(key)

    def move(self, key):
        if self.confused > 0:
            key = random.choice(MOVE_KEYS)
            self.confused -= 1

        if key == K_LEFT or key == K_q:
            self.side = self.left
            if (self.case_x > 0 and
                    self.level[self.case_y][self.case_x - 1] != 'w'):
                self.case_x -= 1
                self.x = self.case_x * SPRITE_SIZE
        elif key == K_RIGHT or key == K_d:
            self.side = self.right
            if (self.case_x < SPRITE_BY_SIDE - 1 and
                    self.level[self.case_y][self.case_x + 1] != 'w'):
                self.case_x += 1
                self.x = self.case_x * SPRITE_SIZE
        elif key == K_UP or key == K_z or key == K_w:
            self.side = self.up
            if (self.case_y > 0 and
                    self.level[self.case_y - 1][self.case_x] != 'w'):
                self.case_y -= 1
                self.y = self.case_y * SPRITE_SIZE
        elif key == K_DOWN or key == K_s:
            self.side = self.down
            if (self.case_y < SPRITE_BY_SIDE - 1 and
                    self.level[self.case_y + 1][self.case_x] != 'w'):
                self.case_y += 1
                self.y = self.case_y * SPRITE_SIZE

    def do_action(self, key):
        if key == K_l:
            self.set_hero_sprites('link')
            self.side = self.down

    def save_score(self):
        with open(DATA_PATH + 'highscore.txt', 'w') as handle:
            handle.write(str(self.highscore))

    def load_highscore(self):
        if os.path.isfile(DATA_PATH + 'highscore.txt') and os.stat(DATA_PATH + 'highscore.txt').st_size > 0:
            with open(DATA_PATH + 'highscore.txt', 'r') as handle:
                return int(handle.read())
        else:
            return 0
