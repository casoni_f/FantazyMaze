#!/usr/bin/python
# -*- coding: utf8 -*-


class Bonus:
    def __init__(self, x, y, points=10, used=False):
        self.x = x
        self.y = y
        self.points = points
        self.used = used

    def on_use(self, player):
        self.used = True
        player.score += self.points
        if player.score > player.highscore:
            player.highscore = player.score
        print(player.score)
