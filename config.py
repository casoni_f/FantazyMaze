#!/usr/bin/python
# -*- coding: utf8 -*-

import platform
from pygame.locals import *

# Game Parameters
FPS = 30
FONT = 'Comic Sans MS'

if platform.system() == 'Windows':
    FONT_SIZE = 20
else:
    FONT_SIZE = 30

SOUNDS = True
VOLUME = 0.7
ENDLESS_MODE_TIMER = 40
DATA_PATH = './data/'

# Keybindings
MOVE_KEYS = (K_UP, K_DOWN, K_RIGHT, K_LEFT, K_q, K_a, K_q, K_s, K_z, K_d, K_w)
MENU_KEYS = (K_m, K_SEMICOLON)
SOUNDS_KEYS = (K_p, K_KP_PLUS, K_KP_MINUS)

# Path
ICONS_PATH = './resources/icons/'
IMAGES_PATH = './resources/images/'
SHEETS_PATH = './resources/sheets/'
SPRITES_PATH = './resources/sprites/'
BONUS_PATH = './resources/sprites/bonuses/'
TRAPS_PATH = './resources/sprites/traps/'
SOUNDS_PATH = './resources/sounds/'

# Window Parameters
TITLE = 'Simple Maze Runner'
ICON = ICONS_PATH + 'icon.png'
SPRITE_BY_SIDE = 31
SPRITE_SIZE = 30
WINDOW_SIZE = SPRITE_BY_SIDE * SPRITE_SIZE

# Heroes
HEROES = {}
HEROES[K_KP1] = ['dk', False]
HEROES[K_KP2] = ['link', True]
HEROES[K_KP3] = ['happy', False]

# Maps
MAPS = {}
MAPS[K_F1] = 'map1'
MAPS[K_F2] = 'map2'
MAPS[K_F3] = 'map3'
MAPS[K_F4] = 'map4'
MAPS[K_F5] = 'map5'
MAPS[K_F6] = 'map6'

# Traps
TRAP_CHARS = ('x', 'r', 'c', 'p', 't')
ENDLESS_TRAPS_CHARS = ('p', 't')
SNAKE_TRAP_ACTIVATED = TRAPS_PATH + 'snake_trap_activated.png'
SPIKE_TRAP_ACTIVATED = TRAPS_PATH + 'spike_trap_activated.png'

# Bonus
BONUS_POINTS = (10, 25, 50)
ANTIDOTE = BONUS_PATH + 'antidote.png'
BONUS = BONUS_PATH + 'bonus.png'

# Level Sprites
BACKGROUND = IMAGES_PATH + 'background.jpg'
HOME = IMAGES_PATH + 'welcome.png'
START = SPRITES_PATH + 'start.png'
WALL = SPRITES_PATH + 'wall.png'
END = SPRITES_PATH + 'end.png'
END_VIEW = IMAGES_PATH + 'end.png'

# Colors
RED = (255, 0, 0)
GREEN = (0, 255, 0)
BLUE = (0, 0, 255)
WHITE = (255, 255, 255)
BLACK = (0, 0, 0)

# Musics & Sounds
TRAP_SOUND_PATH = SOUNDS_PATH + '/traps/'
BONUS_SOUND_PATH = SOUNDS_PATH + '/bonus/'
MAIN_SONG = 'ancientwell.mp3'

# Sounds Lists
TRAP_SOUND_LIST = {}
TRAP_SOUND_LIST['ExplosiveTrap'] = TRAP_SOUND_PATH + 'explosive_trap.wav'
TRAP_SOUND_LIST['RandomizeTrap'] = None
TRAP_SOUND_LIST['ConfusingTrap'] = TRAP_SOUND_PATH + 'confusing_trap.wav'
TRAP_SOUND_LIST['SnakeTrap'] = TRAP_SOUND_PATH + 'snake_trap.wav'
TRAP_SOUND_LIST['SpikeTrap'] = TRAP_SOUND_PATH + 'spike_trap.wav'