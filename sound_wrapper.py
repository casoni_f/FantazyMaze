#!/usr/bin/python
# -*- coding: utf8 -*-

import pygame
from config import *


class SoundWrapper:
    def __init__(self):
        self.paused = not SOUNDS
        self.sound_library = {}
        self.volume = VOLUME

        pygame.mixer.music.load(SOUNDS_PATH + MAIN_SONG)
        pygame.mixer.music.set_volume(self.volume)
        pygame.mixer.music.play(-1)
        if self.paused:
            pygame.mixer.music.pause()

    def toggle_sound(self):
        if self.paused:
            self.paused = False
            pygame.mixer.music.unpause()
        else:
            self.paused = True
            pygame.mixer.music.pause()

    def load_sound(self, name, sound_path):
        try:
            self.sound_library[name] = pygame.mixer.Sound(sound_path)
        except:
            self.sound_library[name] = None

    def load_traps_sound(self, trap_sounds_list):
        for trap, sound_path in trap_sounds_list.items():
            try:
                self.sound_library[trap] = pygame.mixer.Sound(sound_path)
            except:
                self.sound_library[trap] = None

    def up_volume(self, amount=0.10):
        if self.volume + amount < 1.0:
            self.volume += amount
            pygame.mixer.music.set_volume(self.volume)
        else:
            self.volume = 1.0
            pygame.mixer.music.set_volume(1.0)

    def lower_volume(self, amount=0.10):
        if self.volume - amount > 0:
            self.volume -= amount
            pygame.mixer.music.set_volume(self.volume)
        else:
            self.volume = 0
            pygame.mixer.music.set_volume(0)

    def play(self, sound_name):
        if not self.paused and self.sound_library.get(sound_name) is not None:
            self.sound_library.get(sound_name).play()
