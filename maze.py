#!/usr/bin/python
# -*- coding: utf8 -*-


from player import *
from level import *
from sound_wrapper import *


class FantazyMaze:
    def __init__(self):
        pygame.mixer.pre_init(44100, -16, 2, 2048)
        pygame.init()
        pygame.font.init()

        self.state = 1
        self.hero = ['dk', False]
        self.level = None
        self.lvl = None

        self.window = pygame.display.set_mode((WINDOW_SIZE, WINDOW_SIZE))

        self.home = pygame.image.load(HOME).convert()
        self.home = pygame.transform.smoothscale(self.home, (WINDOW_SIZE, WINDOW_SIZE))
        self.background = pygame.image.load(BACKGROUND).convert()
        self.background = pygame.transform.smoothscale(self.background, (WINDOW_SIZE, WINDOW_SIZE))
        self.end = pygame.image.load(END_VIEW).convert()
        self.end = pygame.transform.smoothscale(self.end, (WINDOW_SIZE, WINDOW_SIZE))
        self.icon = pygame.image.load(ICON)

        self.endless = False
        self.time_left = None
        self.endless_start = None

        self.score = 0
        self.font = pygame.font.SysFont(FONT, FONT_SIZE)

        self.sound_wrapper = SoundWrapper()
        self.sound_wrapper.load_traps_sound(TRAP_SOUND_LIST)

        pygame.display.set_icon(self.icon)
        pygame.display.set_caption(TITLE)

    def display_texts(self):
        if self.player.poison > 0:
            life_surface = self.font.render(
                'HP : {0} / {1} Poisoned : {2} move(s) left'.format(str(self.player.actual_hp),
                                                                    str(self.player.max_hp), str(self.player.poison)),
                True, GREEN)
        else:
            life_surface = self.font.render(
                'HP : {0} / {1}'.format(str(self.player.actual_hp), self.player.max_hp),
                True, WHITE)
        self.window.blit(life_surface, (WINDOW_SIZE - life_surface.get_width(), 0))
        if self.endless:
            time_surface = self.font.render(str(self.time_left / 1000), True, WHITE)
            self.window.blit(time_surface, ((WINDOW_SIZE - time_surface.get_width()) / 2, 0))
            score_surface = self.font.render(
                'Score : {0} - Best : {1}'.format(str(self.player.score), str(self.player.highscore)),
                True, WHITE)
            self.window.blit(score_surface, (0, 0))

    def display_game(self):
        self.window.blit(self.background, (0, 0))
        self.level.display(self.window)
        self.window.blit(self.player.side, (self.player.x, self.player.y))
        self.display_texts()
        pygame.display.flip()

    def print_centered(self, txt, txt_color, bg_color=None):
        centered = self.font.render(str(txt), True, txt_color, bg_color)
        centered_rect = centered.get_rect(center=(WINDOW_SIZE / 2, WINDOW_SIZE / 2))
        self.window.blit(centered, centered_rect)
        pygame.display.flip()

    def check_case(self):
        if self.level.data[self.player.case_y][self.player.case_x] in TRAP_CHARS:
            trap = find_item(self.level.trap_list, self.player.case_x, self.player.case_y)
            if trap and trap.activated:
                self.sound_wrapper.play(trap.__class__.__name__)
                trap.on_trigger(self.player, self.level)
        elif self.level.data[self.player.case_y][self.player.case_x] == 'a':
            self.player.poison = -1
            self.level.data[self.player.case_y][self.player.case_x] = '0'
            self.level.free_cases.append([self.player.case_x, self.player.case_y])
        elif self.level.data[self.player.case_y][self.player.case_x] == 'b':
            bonus = find_item(self.level.bonus_list, self.player.case_x, self.player.case_y)
            if bonus and not bonus.used:
                bonus.on_use(self.player)
                self.level.bonus_list.remove(bonus)
        elif self.level.data[self.player.case_y][self.player.case_x] == 'e':
            if self.endless:
                self.level = Level()
                self.level.randomize()
                self.state = 2
                self.score = self.player.score
                self.player.save_score()
                self.time_left += 10000
            else:
                self.state = 4

    def handle_sound(self, key):
        if key == K_p:
            self.sound_wrapper.toggle_sound()
        elif key == K_KP_MINUS:
            self.sound_wrapper.lower_volume()
        elif key == K_KP_PLUS:
            self.sound_wrapper.up_volume()

    def update_timer(self):
        now = pygame.time.get_ticks()
        dt = now - self.endless_start
        self.time_left -= dt
        self.endless_start = now

    def run_game(self):
        while self.state > 0:
            pygame.time.Clock().tick(FPS)

            if self.state == 1:
                self.window.blit(self.home, (0, 0))
                pygame.display.flip()
            elif self.state == 3 and self.endless:
                if self.endless_start is None:
                    self.endless_start = pygame.time.get_ticks()
                    self.time_left = ENDLESS_MODE_TIMER * 1000
                elif self.time_left > 0:
                    self.update_timer()
                    self.display_game()
                if self.time_left <= 0:
                    self.state = 5

            for event in pygame.event.get():
                if event.type == KEYDOWN and event.key in SOUNDS_KEYS:
                    self.handle_sound(event.key)
                elif (event.type == QUIT or
                        (event.type == KEYDOWN and event.key == K_ESCAPE)):
                    self.state = 0
                elif self.state > 1 and event.type == KEYDOWN and event.key in MENU_KEYS:
                    self.state = 1
                    self.level = None
                    if self.endless:
                        self.endless = 0
                        if self.player is not None:
                            self.player.score = 0
                            self.score = 0
                elif self.state == 1 and event.type == KEYDOWN:
                    if K_KP0 <= event.key <= K_KP9:
                        if HEROES.get(event.key):
                            self.hero = HEROES.get(event.key)
                    elif K_F1 <= event.key <= K_F12:
                        if MAPS.get(event.key):
                            self.state = 2
                            self.lvl = MAPS.get(event.key)
                    elif event.key == K_e:
                        self.level = Level()
                        self.level.randomize()
                        self.state = 2
                        self.endless = True
                        self.time_left = ENDLESS_MODE_TIMER * 1000
                elif self.state == 2:
                    if self.level is None:
                        self.level = Level('./levels/' + self.lvl)
                        self.level.generate()
                    self.player = Player(self.level, self.hero, self.score)
                    self.display_game()
                    self.state = 3
                elif self.state == 3 and event.type == KEYDOWN:
                    self.player.do(event.key)
                    if self.player.poison == 0:
                        self.player.poison = -1
                        self.player.actual_hp -= 1
                    self.check_case()
                    self.display_game()
                    if self.player.actual_hp <= 0:
                        self.state = 5
                elif self.state == 4:
                    self.window.blit(self.end, (0, 0))
                    pygame.display.flip()
                    if event.type == KEYDOWN and event.key == K_RETURN:
                        self.state = 1
                        self.level = None
                elif self.state == 5:
                    game_over_string = 'GAME OVER ! Press Enter to continue...'
                    self.print_centered(game_over_string, WHITE, BLACK)
                    if event.type == KEYDOWN and event.key == K_RETURN:
                        self.state = 1
                        self.level = None
                        if self.endless:
                            self.player.save_score()
                            self.endless_start = None
                            self.endless = False
                        self.score = 0

if __name__ == "__main__":
    maze = FantazyMaze()
    maze.run_game()
