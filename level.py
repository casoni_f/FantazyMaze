#!/usr/bin/python
# -*- coding: utf8 -*-

import pygame
from maze_generator import *
from trap import *
from bonus import *


class Level:
    def __init__(self, level_file=None):
        self.file = level_file
        self.data = None
        self.init_x = 0
        self.init_y = 0
        self.trap_list = []
        self.bonus_list = []
        self.free_cases = []

    def generate(self):
        if self.file is None:
            self.randomize()

        with open(self.file, 'r') as lvl:
            lvl_struct = []
            line_nb = 0
            for line in lvl:
                lvl_line = []
                case_nb = 0
                for sprite in line:
                    if sprite != '\n':
                        lvl_line.append(sprite)
                    if sprite == 's':
                        self.init_x = case_nb
                        self.init_y = line_nb
                    elif sprite in TRAP_CHARS:
                        trap = None
                        if sprite == 'x':
                            trap = ExplosiveTrap(case_nb, line_nb)
                        elif sprite == 'r':
                            trap = RandomizeTrap(case_nb, line_nb)
                        elif sprite == 'c':
                            trap = ConfusingTrap(case_nb, line_nb)
                        elif sprite == 'p':
                            trap = SnakeTrap(case_nb, line_nb, TRAP_SOUND_PATH + 'snake_trap.wav')
                        elif sprite == 't':
                            trap = SpikeTrap(case_nb, line_nb, TRAP_SOUND_PATH + 'spike_trap.wav')
                        self.trap_list.append(trap)
                    elif sprite == ' ' or sprite == '0':
                        self.free_cases.append([case_nb, line_nb])
                    case_nb += 1
                lvl_struct.append(lvl_line)
                line_nb += 1
            self.data = lvl_struct

    def generate_endless_bonuses(self, lvl_struct):
        max_bonus = len(self.free_cases) / 40
        while max_bonus > 0:
            bonus_coord = random.choice(self.free_cases)
            lvl_struct[bonus_coord[1]][bonus_coord[0]] = 'b'
            self.free_cases.remove(bonus_coord)
            self.bonus_list.append(Bonus(bonus_coord[0], bonus_coord[1], random.choice(BONUS_POINTS)))
            max_bonus -= 1

    def generate_endless_traps(self, lvl_struct):
        max_traps = len(self.free_cases) / 40
        while max_traps > 0:
            trap_coord = random.choice(self.free_cases)
            trap_char = random.choice(ENDLESS_TRAPS_CHARS)
            self.free_cases.remove(trap_coord)
            lvl_struct[trap_coord[1]][trap_coord[0]] = trap_char
            if trap_char == 'p':
                self.trap_list.append(SnakeTrap(trap_coord[0], trap_coord[1]))
            elif trap_char == 't':
                self.trap_list.append(SpikeTrap(trap_coord[0], trap_coord[1]))
            max_traps -= 1

    def randomize(self):
        lvl_struct = []
        tmp = generate_maze()
        line_nb = 0
        for line in tmp:
            lvl_line = []
            case_nb = 0
            for case in line:
                if line_nb == 1 and case_nb == 1:
                    lvl_line.append('s')
                elif case == 1:
                    lvl_line.append('w')
                else:
                    lvl_line.append('0')
                    self.free_cases.append([case_nb, line_nb])
                case_nb += 1
            lvl_struct.append(lvl_line)
            line_nb += 1
        self.init_x = 1
        self.init_y = 1
        end_coord = random.choice(self.free_cases)
        self.free_cases.remove(end_coord)
        lvl_struct[end_coord[1]][end_coord[0]] = 'e'
        self.generate_endless_bonuses(lvl_struct)
        self.generate_endless_traps(lvl_struct)
        self.data = lvl_struct

    def display(self, window):
        wall = pygame.image.load(WALL).convert_alpha()
        start = pygame.image.load(START).convert_alpha()
        end = pygame.image.load(END).convert_alpha()
        bonus_sprite = pygame.image.load(BONUS).convert_alpha()
        antidote = pygame.image.load(ANTIDOTE).convert_alpha()

        line_nb = 0
        for line in self.data:
            case_nb = 0
            for sprite in line:
                x = case_nb * SPRITE_SIZE
                y = line_nb * SPRITE_SIZE
                if sprite == 's':
                    window.blit(start, (x, y))
                elif sprite == 'w':
                    window.blit(wall, (x, y))
                elif sprite == 'e':
                    window.blit(end, (x, y))
                elif sprite == 'a':
                    window.blit(antidote, (x, y))
                case_nb += 1
            line_nb += 1
        for bonus in self.bonus_list:
            if not bonus.used:
                window.blit(bonus_sprite, (bonus.x * SPRITE_SIZE, bonus.y * SPRITE_SIZE))
        for trap in self.trap_list:
            if not trap.activated and trap.sprite is not None:
                trap_sprite = pygame.image.load(trap.sprite).convert_alpha()
                window.blit(trap_sprite, (trap.x * SPRITE_SIZE, trap.y * SPRITE_SIZE))


def find_item(items, x, y):
    for item in items:
        if item.x == x and item.y == y:
            return item
    return None
